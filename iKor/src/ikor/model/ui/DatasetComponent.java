package ikor.model.ui;

import ikor.model.data.Dataset;

public interface DatasetComponent 
{
	// Dataset
	
	public Dataset getData ();

	public void setData (Dataset data);
	
	
	// Headers
	
	public Label getHeader (int column);
	
	public void setHeader (int column, Label header);
	
	public void setHeader (int column, String text);	
	
	public void addHeader (Label header);
	
	public void addHeader (String text);	
	
	
	// Dataset selection
	
	public DatasetSelection getSelection();

	public void setSelection (DatasetSelection selection); 

	
	// Observer design pattern
	
	public void notifyObservers (Dataset data);
}
