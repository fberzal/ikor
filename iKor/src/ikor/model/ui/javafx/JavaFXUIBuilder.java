package ikor.model.ui.javafx;

import ikor.model.ui.File;
import ikor.model.ui.UI;
import ikor.model.ui.UIModel;
import ikor.model.ui.swing.SwingFileDialog;
import ikor.model.ui.swing.SwingUIBuilder;

public class JavaFXUIBuilder extends SwingUIBuilder 
{

	@Override
	public UI build (UIModel model) 
	{
		if (model instanceof File)
			return new SwingFileDialog((File)model);
		else {
			return JavaFXUI.create(model);
		}
	}
	
}
