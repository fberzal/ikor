package ikor.model.ui.javafx;

import ikor.model.ui.Figure;
import ikor.model.ui.UI;
import ikor.model.ui.UIModel;
import ikor.model.ui.swing.SwingUI;

public class JavaFXUI extends SwingUI  
{

	public JavaFXUI (UIModel context) 
	{
		super(context);
		builders.set ( Figure.class, new JavaFXFigureFactory() );
	}

	
	public static UI create (UIModel model)
	{
		JavaFXUI ui = new JavaFXUI(model);
		
		ui.fill();
		
		return ui;
	}
	
}
