package ikor.model.ui.javafx;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import ikor.model.Observer;
import ikor.model.Subject;
import ikor.model.graphics.ui.JavaFXDrawingComponent;
import ikor.model.ui.Figure;
import ikor.model.ui.UIFactory;

public class JavaFXFigureFactory implements UIFactory<JavaFXUI,Figure> 
{

	@Override
	public void build(JavaFXUI ui, Figure figure) 
	{
		JavaFXDrawingComponent control = new JavaFXDrawingComponent(figure.getDrawing());
		
		if (figure.getTooltipProvider()!=null)
			control.getEventHandler().setTooltipProvider( figure.getTooltipProvider() );
		
		if (figure.getDraggingListener()!=null)
			control.getEventHandler().setDraggingListener( figure.getDraggingListener() );
		
		if (figure.getSelectionListener()!=null)
			control.getEventHandler().setSelectionListener( figure.getSelectionListener() );
		
		control.addComponentListener(new JavaFXDrawingComponentListener(figure, control));
		
		figure.addObserver( new FigureObserver(figure,control) );
		
		ui.addComponent(control);
		
		control.setVisible( figure.isVisible() );
	}
	
	
	// Figure observer
	
	public class FigureObserver implements Observer
	{
		private Figure figure;
		private JavaFXDrawingComponent control;
			
		public FigureObserver (Figure figure, JavaFXDrawingComponent control) 
		{
			this.figure = figure;
			this.control = control;
		}
			
		@Override
		public void update (Subject o, Object arg) 
		{
			control.update();
			control.setVisible(figure.isVisible());
		}
	}
	
	// Component listener
	
	class JavaFXDrawingComponentListener extends ComponentAdapter 
	{	
		private Figure figure;
		private JavaFXDrawingComponent control;
		
		public JavaFXDrawingComponentListener (Figure figure, JavaFXDrawingComponent control) 
		{
			this.figure = figure;
			this.control = control;
		}
		
		@Override
		public void componentResized(ComponentEvent e) 
		{
			figure.setSize(control.getWidth(), control.getHeight());
			figure.update();
			control.update();
		}
	}
	
}
