package ikor.model.ui;

import java.util.Iterator;

import ikor.collection.DynamicSet;
import ikor.collection.Set;
import ikor.model.Subject;
import ikor.util.log.Log;

// Dataset selection

public class DatasetSelection extends Subject<DatasetSelection> implements SelectionListener<Integer>, Iterable<Integer>
{
	Set<Integer> selection = new DynamicSet<Integer>();
	
	
	public int size ()
	{
		return selection.size();
	}
	
	@Override
	public Iterator<Integer> iterator() 
	{
		return selection.iterator();
	}	

	public boolean contains (Integer object)
	{
		return selection.contains(object);
	}
	
	@Override
	public void setSelection(Integer object) 
	{
		clearSelection();
		addSelection(object);
	}

	@Override
	public void addSelection(Integer object) 
	{
		if (object!=null) {
			selection.add(object);
			update(this, this);
		}
	}

	@Override
	public void clearSelection() 
	{
		selection.clear();
		update(this, this);
	}

	// Observer design pattern
	
	private boolean updating = false;
	
	@Override
	public void update (Subject subject, DatasetSelection object) 
	{
		if (!updating) {
			
			try {
				updating = true;
				Log.info("Dataset selection - "+object+" @ "+subject);	
				notifyObservers(this);
			} finally {
				updating = false;
			}
		}
	}

}
