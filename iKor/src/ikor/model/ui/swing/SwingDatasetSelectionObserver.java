package ikor.model.ui.swing;

import ikor.model.Observer;
import ikor.model.Subject;
import ikor.model.ui.DatasetSelection;

import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;


// Observer design pattern

public class SwingDatasetSelectionObserver implements Observer<DatasetSelection>
{
	private SwingDatasetJTable table;
	
	public SwingDatasetSelectionObserver (SwingDatasetJTable table)
	{
		this.table = table;
	}

	@Override
	public void update(Subject<DatasetSelection> subject, DatasetSelection object) 
	{
	    SwingUtilities.invokeLater(new Runnable() 
	    {
	      public void run()
	      {
	    	  ListSelectionModel lsm = table.getSelectionModel();
	    	  DatasetSelection selection = object;
	    	  	    	  	    	  
	    	  lsm.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
	    	  lsm.setValueIsAdjusting(true);
	    	  
	    	  for (int element: selection) {
	    		  int index = table.convertRowIndexToView(element);
	    		  lsm.addSelectionInterval(index,index);
	    	  }
	      }
	    });			
	}
}	
