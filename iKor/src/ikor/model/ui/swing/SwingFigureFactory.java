package ikor.model.ui.swing;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import ikor.model.Observer;
import ikor.model.Subject;
import ikor.model.graphics.ui.SwingDrawingComponent;
import ikor.model.ui.Figure;
import ikor.model.ui.UIFactory;

import javax.swing.SwingUtilities;



public class SwingFigureFactory implements UIFactory<SwingUI,Figure>
{
	@Override
	public void build (SwingUI ui, Figure figure) 
	{
		SwingDrawingComponent control = new SwingDrawingComponent(figure.getDrawing());
		
		if (figure.getTooltipProvider()!=null)
			control.getEventHandler().setTooltipProvider( figure.getTooltipProvider() );
		
		if (figure.getDraggingListener()!=null)
			control.getEventHandler().setDraggingListener( figure.getDraggingListener() );
		
		if (figure.getSelectionListener()!=null)
			control.getEventHandler().setSelectionListener( figure.getSelectionListener() );
		
		control.addComponentListener( new SwingDrawingComponentListener(figure, control) );
		
		figure.addObserver( new FigureObserver(figure,control) );
		
		ui.addComponent ( control );	
		
		control.setVisible( figure.isVisible() );
	}
	
	// Figure observer
	
	public class FigureObserver implements Observer
	{
		private Figure figure;
		private SwingDrawingComponent control;
		
		public FigureObserver (Figure figure, SwingDrawingComponent control)
		{
			this.figure = figure;
			this.control = control;
		}

		@Override
		public void update(Subject o, Object arg) 
		{
		    SwingUtilities.invokeLater(new Runnable() 
		    {
		      public void run()
		      {
		    	  control.setDrawing(figure.getDrawing());
		    	  control.setVisible(figure.isVisible());
		    	  control.repaint();
		      }
		    });			
		}
	}
	
	// Component listener
	
	public class SwingDrawingComponentListener extends ComponentAdapter
	{
		private Figure figure;
		private SwingDrawingComponent control;

		public SwingDrawingComponentListener (Figure figure, SwingDrawingComponent control)
		{
			this.figure = figure;
			this.control = control;
		}
		
		@Override
		public void componentResized(ComponentEvent e) 
		{
			figure.setSize(control.getWidth(), control.getHeight());
			figure.update();
			
		    SwingUtilities.invokeLater(new Runnable() 
		    {
		      public void run()
		      {
		    	  control.setVisible(figure.isVisible());
		    	  control.repaint();
		      }
		    });						
		}

	}
	
}
