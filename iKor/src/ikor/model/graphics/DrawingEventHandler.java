package ikor.model.graphics;

/**
 * Drawing event handling
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class DrawingEventHandler 
{
	// Event handling
	
	private DrawingTooltipProvider tooltipProvider;
	private DrawingSelectionListener selectionListener;
	private DrawingUpdateListener actionListener;
	private DrawingUpdateListener draggingListener;

	// Getters & setters
	
	public DrawingTooltipProvider getTooltipProvider() 
	{
		return tooltipProvider;
	}

	public void setTooltipProvider(DrawingTooltipProvider tooltipProvider) 
	{
		this.tooltipProvider = tooltipProvider;
	}
	
	
	public DrawingSelectionListener getSelectionListener() 
	{
		return selectionListener;
	}

	public void setSelectionListener(DrawingSelectionListener selectionListener) 
	{
		this.selectionListener = selectionListener;
	}


	public DrawingUpdateListener getActionListener() 
	{
		return actionListener;
	}

	public void setActionListener(DrawingUpdateListener actionListener) 
	{
		this.actionListener = actionListener;
	}


	public DrawingUpdateListener getDraggingListener() 
	{
		return draggingListener;
	}

	public void setDraggingListener(DrawingUpdateListener draggingListener) 
	{
		this.draggingListener = draggingListener;
	}	
	
}
