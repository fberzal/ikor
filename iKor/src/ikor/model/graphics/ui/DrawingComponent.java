package ikor.model.graphics.ui;

import ikor.model.graphics.Drawing;
import ikor.model.graphics.DrawingEventHandler;

/**
 * Drawing component interface
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public interface DrawingComponent 
{

	/**
	 * Get current drawing.
	 * 
	 * @return the drawing
	 */
	public Drawing getDrawing(); 

	/**
	 * Set drawing.
	 * 
	 * @param drawing the drawing to display
	 */
	public void setDrawing(Drawing drawing); 

	/**
	 * Get event handler.
	 * 
	 * @return Drawing event handler
	 */
	public DrawingEventHandler getEventHandler ();
	
}
