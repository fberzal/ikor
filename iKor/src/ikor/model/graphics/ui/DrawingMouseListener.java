package ikor.model.graphics.ui;


import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.event.MouseInputListener;

import ikor.model.graphics.Drawing;
import ikor.model.graphics.DrawingElement;
import ikor.model.graphics.DrawingEventHandler;


/**
 * Drawing component mouse event handling
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */

public class DrawingMouseListener implements MouseInputListener
{
	private JComponent control;
	private Drawing drawing;
	private DrawingEventHandler eventHandler;
	
	private DrawingElement draggedElement;
	
	public DrawingMouseListener (JComponent control, Drawing drawing, DrawingEventHandler eventHandler)
	{
		this.control = control;
		this.drawing = drawing;
		this.eventHandler = eventHandler;
	}

	@Override
	public void mouseClicked(MouseEvent e) 
	{	
		if (eventHandler.getSelectionListener()!=null) {
			
			updateSelection(e);
			
		} else if (eventHandler.getActionListener()!=null) {
			
			int x = e.getX();
			int y = e.getY();
			String id = drawing.getElement(x,y);
			
			if (id!=null) {
				eventHandler.getActionListener().update(id,x,y);
				control.repaint();
			}
		}
	}

	
	@Override
	public void mousePressed(MouseEvent e)
	{			
		if (eventHandler.getSelectionListener()!=null)
			updateSelection(e);
		
		if (eventHandler.getDraggingListener()!=null)
			draggedElement = drawing.getDrawingElement( e.getX(), e.getY() );
	}

	@Override
	public void mouseReleased(MouseEvent e) 
	{
		draggedElement = null;
	}

	@Override
	public void mouseEntered(MouseEvent e) 
	{
	}

	@Override
	public void mouseExited(MouseEvent e) 
	{
	}

	@Override
	public void mouseDragged(MouseEvent e) 
	{
		mouseMoved(e);
	}

	@Override
	public void mouseMoved(MouseEvent e) 
	{
		if ((eventHandler.getDraggingListener()!=null) && (draggedElement!=null)) {
			
			if (draggedElement.getId()!=null) {
				eventHandler.getDraggingListener().update( draggedElement.getId(), e.getX(), e.getY());
				control.repaint();
			}
			
		} else {  // Update tooltip
			
			String id;
			String tooltip;			

			if (eventHandler.getTooltipProvider()!=null) {

				control.setToolTipText("");

				id = drawing.getElement( e.getX(), e.getY());

				if (id!=null) {
					tooltip = eventHandler.getTooltipProvider().get(id);

					if (tooltip!=null)
						control.setToolTipText(tooltip);
				}
			}
		}
	}


	private void updateSelection(MouseEvent e) 
	{
		int x = e.getX();
		int y = e.getY();
	
		String id = drawing.getElement(x,y);
		
		if (e.isControlDown())
			eventHandler.getSelectionListener().addSelection(id);
		else
			eventHandler.getSelectionListener().setSelection(id);
		
		control.repaint();
	}
	
}
