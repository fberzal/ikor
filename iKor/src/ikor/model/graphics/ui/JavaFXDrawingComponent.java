package ikor.model.graphics.ui;

import ikor.model.graphics.Bitmap;
import ikor.model.graphics.Drawing;
import ikor.model.graphics.DrawingElement;
import ikor.model.graphics.DrawingEventHandler;
import ikor.model.graphics.Style;
import ikor.model.graphics.styles.FontStyle;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;


/**
 * JavaFX drawing component
 * 
 * @author Victor Martinez (victormg@acm.org) & Fernando Berzal (berzal@acm.org)
 */
public class JavaFXDrawingComponent extends JFXPanel implements DrawingComponent
{
	private Drawing drawing;

	private DrawingEventHandler eventHandler;
	
	private Group shapes;
	private Scene scene;

	private Map<DrawingElement, ShapeEntry> element2shape ;
	private Map<String, ImageEntry> url2image;

	// Constructor

	public JavaFXDrawingComponent (Drawing drawing) 
	{
		this.drawing = drawing;
		this.eventHandler = new DrawingEventHandler();
		
		this.element2shape = new HashMap();
		this.url2image = new HashMap();

		this.setPreferredSize(new Dimension(drawing.getWidth(), drawing.getHeight()));

		DrawingMouseListener mouseListener = new DrawingMouseListener(this,drawing,eventHandler);
		this.addMouseListener(mouseListener);
		this.addMouseMotionListener(mouseListener);
		
		createScene();
		update();
	}


	public Drawing getDrawing() 
	{
		return drawing;
	}
	
	public void setDrawing(Drawing drawing) 
	{
		this.drawing = drawing;
		this.setPreferredSize(new Dimension(drawing.getWidth(), drawing.getHeight()));
	}
	

	public DrawingEventHandler getEventHandler()
	{
		return this.eventHandler;
	}
	
	
	// Display

	@Override
	public void paintComponent (Graphics g)
    {
		super.paintComponent(g);
		update();
    }	
	
	
	private void createScene() 
	{
		Platform.runLater(() -> {
			shapes = new Group();
			scene = new Scene(shapes, Color.TRANSPARENT);
			setScene(scene);
		});
	}

	public void update() 
	{
		Platform.runLater(() -> updateElements() );
	}
	
	public void updateElements() 
	{
		// Update existing figures
		for (DrawingElement element : drawing.getElements()) {
			ShapeEntry entry = getEntry(element);
			updateShape(entry.shape, element, entry);
			updateStyle(entry.shape, element, entry);
		}

		// Clear non-existing figures
		Iterator<Entry<DrawingElement, ShapeEntry>> iterShape = element2shape.entrySet().iterator();
		while (iterShape.hasNext()) {
			Entry<DrawingElement, ShapeEntry> entry = iterShape.next();
			ShapeEntry mapEntry = entry.getValue();
			if (!mapEntry.present) {
				shapes.getChildren().remove(mapEntry.shape);
				iterShape.remove();
			} else {
				mapEntry.present = false;
			}
		}

		// Clear unused images
		Iterator<Entry<String, ImageEntry>> iterImg = url2image.entrySet().iterator();
		while (iterImg.hasNext()) {
			Entry<String, ImageEntry> entry = iterImg.next();
			ImageEntry mapEntry = entry.getValue();
			if (!mapEntry.used) {
				iterImg.remove();
			} else {
				mapEntry.used = false;
			}
		}
	}

	private void updateShape (Shape shape, DrawingElement element, ShapeEntry entry) 
	{
		entry.present = true;
		if (element instanceof ikor.model.graphics.Line)
			updateLine((Line) shape, (ikor.model.graphics.Line) element);
		else if (element instanceof ikor.model.graphics.Rectangle)
			updateRectangle((Rectangle) shape, (ikor.model.graphics.Rectangle) element);
		else if (element instanceof ikor.model.graphics.Polygon)
			updatePolygon((Polygon) shape, (ikor.model.graphics.Polygon) element);
		else if (element instanceof ikor.model.graphics.Circle)
			updateCircle((Circle) shape, (ikor.model.graphics.Circle) element);
		else if (element instanceof ikor.model.graphics.Arc)
			updateArc((Arc) shape, (ikor.model.graphics.Arc) element);
		else if (element instanceof ikor.model.graphics.Ellipse)
			updateEllipse((Ellipse) shape, (ikor.model.graphics.Ellipse) element);
		else if (element instanceof ikor.model.graphics.Bitmap)
			updateBitmap((Rectangle) shape, (Bitmap) element);
		else if (element instanceof ikor.model.graphics.Text)
			updateText((Text) shape, (ikor.model.graphics.Text) element);
	}

	private void updateStyle (Shape shape, DrawingElement element, ShapeEntry entry) 
	{
		Style style = element.getStyle();
		Style border = element.getBorder();

		if (element instanceof ikor.model.graphics.Bitmap) {
			
			Bitmap bitmap = (Bitmap) element;
			String url = bitmap.getUrl();
			ImageEntry imgEntry;

			if (url2image.containsKey(url)) {
				imgEntry = url2image.get(url);
			} else {
				imgEntry = new ImageEntry(url);
				url2image.put(url, imgEntry);
			}

			shape.setFill(imgEntry.pattern);
			imgEntry.used = true;

		} else {
			
			if (element instanceof ikor.model.graphics.Line) {
				
				if (style != null && style != entry.lastStyle) {
					shape.setStroke(getColor(style.getColor()));
					shape.setStrokeWidth(style.getWidth());
				}
				
			} else {
				
				if (style != null && style != entry.lastStyle) {
					shape.setFill(styleToPaint(style));
				}

				if (border != null && style != entry.lastBorder) {
					shape.setStroke(getColor(border.getColor()));
					shape.setStrokeWidth(border.getWidth());
				}
			}

			if (entry.lastStyle != style)
				entry.lastStyle = style;
			if (entry.lastBorder != border)
				entry.lastBorder = border;
		}
	}

	// Line
	private void updateLine (Line shape, ikor.model.graphics.Line line) 
	{
		shape.setStartX(line.getStartX());
		shape.setStartY(line.getStartY());
		shape.setEndX(line.getEndX());
		shape.setEndY(line.getEndY());
	}

	// Rectangle
	private void updateRectangle (Rectangle shape, ikor.model.graphics.Rectangle rectangle) 
	{
		shape.setX(rectangle.getX());
		shape.setY(rectangle.getY());
		shape.setWidth(rectangle.getWidth());
		shape.setHeight(rectangle.getHeight());
		
		rotate(shape, rectangle.getX(), rectangle.getY(), rectangle.getRotation());
	}

	// Polygon
	private void updatePolygon (Polygon shape, ikor.model.graphics.Polygon polygon) 
	{
		shape.getPoints().clear();
		
		for (int i = 0; i < polygon.getXCoords().length; i++) {
			shape.getPoints().add((double) polygon.getXCoords()[i]);
			shape.getPoints().add((double) polygon.getYCoords()[i]);
		}
		
		rotate(shape, polygon.getX(), polygon.getY(), polygon.getRotation());
	}

	// Circle
	private void updateCircle (Circle shape, ikor.model.graphics.Circle circle) 
	{
		shape.setCenterX(circle.getCenterX());
		shape.setCenterY(circle.getCenterY());
		shape.setRadius(circle.getRadius());
	}


	// Ellipse
	private void updateEllipse (Ellipse shape, ikor.model.graphics.Ellipse ellipse) 
	{
		shape.setCenterX(ellipse.getCenterX());
		shape.setCenterY(ellipse.getCenterY());
		
		shape.setRadiusX(ellipse.getRadiusX());
		shape.setRadiusY(ellipse.getRadiusY());
		
		rotate(shape, ellipse.getCenterX(), ellipse.getCenterY(), ellipse.getRotation() );
	}

	// Arc
	private void updateArc (Arc shape, ikor.model.graphics.Arc arc) 
	{
		shape.setCenterX(arc.getCenterX());
		shape.setCenterY(arc.getCenterY());

		shape.setStartAngle(180*arc.getStartAngle()/Math.PI);
		shape.setLength(180*arc.getExtent()/Math.PI);
		
		shape.setRadiusX(arc.getRadiusX());
		shape.setRadiusY(arc.getRadiusY());
		
		shape.setType(ArcType.ROUND);

		rotate(shape, arc.getCenterX(), arc.getCenterY(), arc.getRotation() );
	}

	// Bitmap
	private void updateBitmap(Rectangle bitmap, Bitmap element) 
	{
		bitmap.setX(element.getX());
		bitmap.setY(element.getY());
		bitmap.setRotate(element.getRotation());
		bitmap.setWidth(element.getWidth());
		bitmap.setHeight(element.getHeight());
	}

	// Text
	private void updateText(Text text, ikor.model.graphics.Text element) 
	{
		text.setX(element.getX());
		text.setY(element.getY());
		text.setText(element.getText());
		
		if (element.getStyle() instanceof FontStyle) {
			
			// Text style
			
			java.awt.Font font = ((FontStyle) element.getStyle()).getFont();
			text.setFont( Font.font(font.getName(), font.getSize()) );
			
			// Rotation 
			
			rotate(text, element.getX(), element.getY(), ((FontStyle) element.getStyle()).getAngle() );
		}
	}
	
	private void rotate (Shape shape, int x, int y, double angle)
	{
		shape.getTransforms().clear();
		
		if (angle!=0) {
			// Rotation around custom pivot
			Rotate rotate = new Rotate();
			rotate.setPivotX(x);                // Pivot X
			rotate.setPivotY(y);                // Pivot Y
			rotate.setAngle(180*angle/Math.PI); // Angle degrees
			shape.getTransforms().add(rotate);
			// vs. around center, i.e. shape.setRotate( angle );
		}
	}

	private Color getColor(java.awt.Color color) 
	{
		return Color.color (
				color.getRed() / 255.0, 
				color.getGreen() / 255.0, 
				color.getBlue() / 255.0,
				color.getAlpha() / 255.0);
	}

	private Paint styleToPaint(Style style) 
	{
		if (style instanceof ikor.model.graphics.styles.LinearGradient)
			return createLinearGradient(style);
		else if (style instanceof ikor.model.graphics.styles.RadialGradient)
			return createRadialGradient(style);
		else
			return getColor(style.getColor());
	}

	private LinearGradient createLinearGradient(Style style) 
	{
		ikor.model.graphics.styles.LinearGradient gradient = (ikor.model.graphics.styles.LinearGradient) style;

		Stop[] stops = new Stop[gradient.getKeyframeCount()];
		for (int i = 0; i < gradient.getKeyframeCount(); i++)
			stops[i] = new Stop(i, getColor(gradient.getKeyframe(i).getColor()));

		return new LinearGradient (
				gradient.getStartX(), gradient.getStartY(), 
				gradient.getEndX(), gradient.getEndY(),
				true, CycleMethod.NO_CYCLE, stops);
	}

	private RadialGradient createRadialGradient(Style style) 
	{
		ikor.model.graphics.styles.RadialGradient gradient = (ikor.model.graphics.styles.RadialGradient) style;

		Stop[] stops = new Stop[gradient.getKeyframeCount()];
		for (int i = 0; i < gradient.getKeyframeCount(); i++)
			stops[i] = new Stop(i, getColor(gradient.getKeyframe(i).getColor()));

		return new RadialGradient (
				0.0, 0.0, 
				gradient.getCenterX(), gradient.getCenterY(),
				gradient.getRadius(), 
				true, CycleMethod.NO_CYCLE, stops);
	}

	private ShapeEntry getEntry(DrawingElement element) 
	{
		if (element2shape.containsKey(element)) {
			return element2shape.get(element);
		} else {
			Shape shape = null;
			if (element instanceof ikor.model.graphics.Line)
				shape = new Line();
			else if (element instanceof ikor.model.graphics.Rectangle)
				shape = new Rectangle();
			else if (element instanceof ikor.model.graphics.Polygon)
				shape = new Polygon();
			else if (element instanceof ikor.model.graphics.Circle)
				shape = new Circle();
			else if (element instanceof ikor.model.graphics.Arc)
				shape = new Arc();
			else if (element instanceof ikor.model.graphics.Ellipse)
				shape = new Ellipse();
			else if (element instanceof ikor.model.graphics.Bitmap)
				shape = new Rectangle();
			else if (element instanceof ikor.model.graphics.Text)
				shape = new Text();

			shape.setId(element.getId());
			shape.setDisable(true);
			shapes.getChildren().add(shape);
			ShapeEntry entry = new ShapeEntry(shape);
			element2shape.put(element, entry);
			return entry;
		}
	}

	private static class ShapeEntry 
	{
		public Shape shape;
		public Style lastStyle;
		public Style lastBorder;
		public boolean present;

		public ShapeEntry(Shape shape) 
		{
			this.shape = shape;
			this.present = true;
		}
	}

	private static class ImageEntry 
	{
		public ImagePattern pattern;
		public boolean used;

		public ImageEntry(String image) 
		{
			this.pattern = new ImagePattern(new Image(image));
			this.used = true;
		}
	}

}
