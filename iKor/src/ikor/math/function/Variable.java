package ikor.math.function;

public class Variable extends Function 
{
	private String id;
	private double value;
	
	public Variable (String id)
	{
		this.id = id;
	}
	
	public String getID ()
	{
		return id;
	}
	
	public void setValue (double value)
	{
		this.value = value;
	}

	@Override
	public double eval() 
	{
		return value;
	}

	@Override
	public Function diff (Variable v) 
	{
		return (v==this) ? new Constant(1): new Constant(0);
	}

	@Override
	public String toString ()
	{
		return getID();
	}
}
