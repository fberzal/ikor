package ikor.math.function.arithmetic;

import ikor.math.function.BinaryFunction;
import ikor.math.function.Function;
import ikor.math.function.Variable;

public class Division extends BinaryFunction 
{
	public Division (Function left, Function right)
	{
		super(left, right);
	}

	@Override
	public double eval() 
	{
		return left.eval()/right.eval();
	}

	@Override
	public Function diff(Variable v) 
	{
		return (left==right) ? ff.zero(): left.mul(right.inv()).diff(v);
	}
}
