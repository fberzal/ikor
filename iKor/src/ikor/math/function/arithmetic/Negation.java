package ikor.math.function.arithmetic;

import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

public class Negation extends UnaryFunction 
{
	public Negation (Function arg)
	{
		super(arg);
	}

	@Override
	public double eval() 
	{
		return -arg.eval();
	}

	@Override
	public Function diff(Variable v) 
	{
		return ff.neg( arg.diff(v) );
	}

}
