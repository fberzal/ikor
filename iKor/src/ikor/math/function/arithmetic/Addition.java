package ikor.math.function.arithmetic;

import ikor.math.function.BinaryFunction;
import ikor.math.function.Function;
import ikor.math.function.Variable;

public class Addition extends BinaryFunction 
{
	public Addition (Function left, Function right)
	{
		super(left, right);
	}

	@Override
	public double eval() 
	{
		return left.eval()+right.eval();
	}

	@Override
	public Function diff(Variable v) 
	{
		return (left==right) ? ff.val(2).mul( left.diff(v) ): left.diff(v).add(right.diff(v));
	}

}
