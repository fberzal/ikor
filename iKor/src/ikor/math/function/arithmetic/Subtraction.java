package ikor.math.function.arithmetic;

import ikor.math.function.BinaryFunction;
import ikor.math.function.Function;
import ikor.math.function.Variable;

public class Subtraction extends BinaryFunction 
{
	public Subtraction (Function left, Function right)
	{
		super(left, right);
	}

	@Override
	public double eval()
	{
		return left.eval() - right.eval();
	}

	@Override
	public Function diff(Variable v) 
	{
		return (left==right)? ff.zero(): left.diff(v).sub( right.diff(v) );
	}

}
