package ikor.math.function.arithmetic;

import ikor.math.function.BinaryFunction;
import ikor.math.function.Function;
import ikor.math.function.Variable;

public class Multiplication extends BinaryFunction 
{
	public Multiplication (Function left, Function right)
	{
		super(left, right);
	}

	@Override
	public double eval() 
	{
		return left.eval()*right.eval();
	}

	@Override
	public Function diff(Variable v) 
	{
		return (left==right) ? ff.val(2).mul(left.diff(v)).mul(right): (left.diff(v).mul(right)).add(left.mul(right.diff(v)));
	}
}
