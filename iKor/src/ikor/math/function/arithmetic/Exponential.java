package ikor.math.function.arithmetic;

import ikor.math.function.UnaryFunction;
import ikor.math.function.Function;
import ikor.math.function.Variable;

public class Exponential extends UnaryFunction 
{
	public Exponential (Function exponent)
	{
		super(exponent);
	}

	@Override
	public double eval() 
	{
		return Math.exp( arg.eval());
	}

	@Override
	public Function diff(Variable v) 
	{
		return ff.exp(arg).mul(arg.diff(v));
	}
}
