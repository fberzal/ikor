package ikor.math.function.arithmetic;

import ikor.math.function.BinaryFunction;
import ikor.math.function.Constant;
import ikor.math.function.Function;
import ikor.math.function.Variable;

public class Power extends BinaryFunction 
{
	public Power (Function left, Function right)
	{
		super(left, right);
	}

	@Override
	public double eval() 
	{
		return Math.pow( left.eval(), right.eval() );
	}

	@Override
	public Function diff(Variable v) 
	{
		Constant y = ff.val(right.eval()-1);
		
		return right.mul( left.pow(y) ).mul(left.diff(v));
	}
}
