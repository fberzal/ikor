package ikor.math.function.arithmetic;

import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

/**
 * Polynomial term.
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class Term extends UnaryFunction  
{
	private double coefficient;
	private double exponent;
	
	public Term (double coefficient, Function x, double exponent)
	{
		super(x);
		
		this.coefficient = coefficient;
		this.exponent = exponent;
	}

	@Override
	public double eval() 
	{
		return coefficient * Math.pow( arg.eval(), exponent);
	}

	@Override
	public Function diff(Variable v) 
	{
		return new Term ( coefficient*exponent, arg, exponent-1 ).mul( arg.diff(v) );
	}
	

}
