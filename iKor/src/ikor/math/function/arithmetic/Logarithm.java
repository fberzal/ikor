package ikor.math.function.arithmetic;

import ikor.math.function.UnaryFunction;
import ikor.math.function.Function;
import ikor.math.function.Variable;

public class Logarithm extends UnaryFunction 
{
	public Logarithm (Function exponent)
	{
		super(exponent);
	}

	@Override
	public double eval() 
	{
		return Math.log( arg.eval());
	}

	@Override
	public Function diff(Variable v) 
	{
		return arg.inv().mul(arg.diff(v));
	}
}
