package ikor.math.function.arithmetic;

import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

public class Inverse extends UnaryFunction 
{

	public Inverse (Function arg)
	{
		super(arg);
	}
	
	@Override
	public double eval() 
	{
		return 1.0 / arg.eval();
	}

	@Override
	public Function diff(Variable v) 
	{
		return new Term(-1, arg, -2).mul( arg.diff(v) );
	}

}
