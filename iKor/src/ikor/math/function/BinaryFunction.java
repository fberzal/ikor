package ikor.math.function;

public abstract class BinaryFunction extends Function 
{
	protected Function left;
	protected Function right;
	
	public BinaryFunction (Function left, Function right)
	{
		this.left = left;
		this.right = right;
	}

	public Function left ()
	{
		return left;
	}
	
	public Function right ()
	{
		return right;
	}
}
