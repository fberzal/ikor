package ikor.math.function;

public abstract class Function 
{
	protected FunctionFactory ff = new FunctionFactory();
	
	public abstract double eval();
	
	public abstract Function diff(Variable v);
	
	// Utilities
	
	public Function add (Function f)
	{
		return ff.add(this, f);
	}
	
	public Function sub (Function f)
	{
		return ff.sub(this, f);
	}
	
	public Function neg ()
	{
		return ff.neg(this);
	}
	
	public Function mul (Function f)
	{
		return ff.mul(this, f);
	}
	
	public Function div (Function f)
	{
		return ff.div(this, f);
	}
	
	public Function inv ()
	{
		return ff.inv(this);
	}
	
	public Function pow (Function f)
	{
		return ff.pow(this, f);
	}
}
