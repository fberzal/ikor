package ikor.math.function.special;

import ikor.math.Functions;
import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

/**
 * Error function (erf).
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ErrorFunction extends UnaryFunction 
{
	public ErrorFunction (Function arg)
	{
		super(arg);
	}

	@Override
	public double eval() 
	{
		return Functions.erf(arg.eval());
	}

	/**
	 * df/dz = 2/sqrt(pi) * exp(-z^2)
	 * 
	 * @param v Variable
	 * @return Derivative with respect to v
	 */
	@Override
	public Function diff(Variable v) 
	{
		return ff.val(2.0/Math.sqrt(Math.PI)).mul( ff.exp( ff.square(arg).neg() ) ).mul(arg.diff(v));
	}
}
