package ikor.math.function.special;

import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

/**
 * Standard logistic function.
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class Logistic extends UnaryFunction 
{
	public Logistic (Function arg)
	{
		super(arg);
	}

	@Override
	public double eval() 
	{
		return 1 / (1 + Math.exp(-arg.eval()));
	}

	/**
	 * df/dx = f(x) (1 - f(x))
	 * 
	 * @param v Variable
	 * @return Derivative with respect to v
	 */
	@Override
	public Function diff(Variable v) 
	{
		return this.mul( ff.one().sub(this) ).mul( arg.diff(v) );
	}
}
