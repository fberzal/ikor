package ikor.math.function;

public abstract class UnaryFunction extends Function 
{
	protected Function arg;
	
	public UnaryFunction (Function arg)
	{
		this.arg = arg;
	}

	public Function arg ()
	{
		return arg;
	}
}
