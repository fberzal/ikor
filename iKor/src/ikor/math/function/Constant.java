package ikor.math.function;

public class Constant extends Function 
{
	private double constant;
	
	public Constant (double constant)
	{
		this.constant = constant;
	}

	@Override
	public double eval() 
	{
		return constant;
	}

	@Override
	public Function diff(Variable v) 
	{
		return (constant!=0)? new Constant(0): this;
	}
	
	@Override
	public String toString ()
	{
		return ""+constant;
	}

}
