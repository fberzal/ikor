package ikor.math.function;

import ikor.math.function.arithmetic.*;
import ikor.math.function.trigonometric.*;

public class FunctionFactory 
{
	public Constant val (double x)
	{
		return new Constant(x);
	}
	
	public Constant zero ()
	{
		return new Constant(0);
	}
	
	public Constant one ()
	{
		return new Constant(1);
	}
	
	public Variable var (String id)
	{
		return new Variable(id);
	}
	
	public Function add (Function left, Function right)
	{
		return new Addition(left,right);
	}

	public Function sub (Function left, Function right)
	{
		return new Subtraction(left,right);
	}

	public Function neg (Function f)
	{
		return new Negation(f);
	}
	
	public Function mul (Function left, Function right)
	{
		return new Multiplication(left,right);
	}
	
	public Function div (Function left, Function right)
	{
		return new Division(left,right);
	}

	public Function inv (Function f)
	{
		return new Inverse(f);
	}

	public Function exp (Function f)
	{
		return new Exponential(f);
	}
	
	public Function log (Function f)
	{
		return new Logarithm(f);
	}
	
	public Function pow (Function base, Function exponent)
	{
		return new Power(base, exponent);
	}

	public Function term (double coefficient, Function base, double exponent)
	{
		return new Term (coefficient, base, exponent);
	}
	
	public Function square (Function f)
	{
		return new Term (1,f,2);
	}
	
	public Function sqrt (Function f)
	{
		return new Term (1,f,0.5);
	}
	
	// Trigonometric functions
	
	public Function sin (Function f)
	{
		return new Sine(f);
	}
	
	public Function cos (Function f)
	{
		return new Cosine(f);
	}
	
	public Function tan (Function f)
	{
		return new Tangent(f);
	}

	public Function sinh (Function f) 
	{
		return new HyperbolicSine(f);
	}

	public Function cosh (Function f) 
	{
		return new HyperbolicCosine(f);
	}

	public Function tanh (Function f) 
	{
		return new HyperbolicTangent(f);
	}

}
