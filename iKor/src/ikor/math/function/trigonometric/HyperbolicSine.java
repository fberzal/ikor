package ikor.math.function.trigonometric;

import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

public class HyperbolicSine extends UnaryFunction
{
	public HyperbolicSine (Function arg)
	{
		super (arg);
	}

	
	@Override
	public double eval() 
	{
		double x = arg.eval();
		
		return (Math.exp(x)-Math.exp(-x)) / 2.0;
	}

	@Override
	public Function diff(Variable v) 
	{
		return ff.cosh(arg).mul(arg.diff(v));
	}

}
