package ikor.math.function.trigonometric;

import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

public class HyperbolicTangent extends UnaryFunction
{
	public HyperbolicTangent (Function arg)
	{
		super (arg);
	}

	
	@Override
	public double eval() 
	{
		double x = arg.eval();
		double ex = Math.exp(x);
		double emx = Math.exp(-x);
		
		return (ex-emx)/(ex+emx);
	}

	@Override
	public Function diff(Variable v) 
	{
		return ff.term(1, ff.cosh(arg), -2).mul(arg.diff(v));
	}

}
