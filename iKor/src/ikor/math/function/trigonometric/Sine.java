package ikor.math.function.trigonometric;

import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

public class Sine extends UnaryFunction
{
	public Sine (Function arg)
	{
		super (arg);
	}

	
	@Override
	public double eval() 
	{
		return Math.sin(arg.eval());
	}

	@Override
	public Function diff(Variable v) 
	{
		return ff.cos(arg).mul(arg.diff(v));
	}

}
