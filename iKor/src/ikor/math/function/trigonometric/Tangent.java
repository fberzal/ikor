package ikor.math.function.trigonometric;

import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

public class Tangent extends UnaryFunction
{
	public Tangent (Function arg)
	{
		super (arg);
	}

	
	@Override
	public double eval() 
	{
		return Math.tan(arg.eval());
	}

	@Override
	public Function diff(Variable v) 
	{
		return ff.term(1, ff.cos(arg), -2).mul(arg.diff(v));
	}

}
