package ikor.math.function.trigonometric;

import ikor.math.function.Function;
import ikor.math.function.UnaryFunction;
import ikor.math.function.Variable;

public class Cosine extends UnaryFunction
{
	public Cosine (Function arg)
	{
		super (arg);
	}

	
	@Override
	public double eval() 
	{
		return Math.cos(arg.eval());
	}

	@Override
	public Function diff(Variable v) 
	{
		return ff.sin(arg).mul(arg.diff(v)).neg();
	}

}
