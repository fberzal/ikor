package test.ikor.collection;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * iKor collection framework test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { ListTest.class,
					   PriorityQueueTest.class,
					   IndexedPriorityQueueTest.class,
					   test.ikor.collection.index.AllTests.class,
	                   test.ikor.collection.graph.AllTests.class })
public class AllTests {

}