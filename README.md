![iKor](https://bitbucket.org/repo/zKEErR/images/2480880881-logo_animated.gif)

# The iKor reusable component library #

The **iKor library** provides, among other things, a customizable collection framework, support for the execution of parallel algorithms, mathematical routines, and and application generator that can be used to build graphical user interfaces.


# Software license #

The **iKor library** is distributed under the Simplified BSD License below:


> Copyright (c) 2015, Fernando Berzal (berzal@acm.org).
> All rights reserved.
>
> Redistribution and use in source and binary forms, with or without
> modification, are permitted provided that the following conditions are met:
>
> 1. Redistributions of source code must retain the above copyright notice, this
>    list of conditions and the following disclaimer.
> 2. Redistributions in binary form must reproduce the above copyright notice,
>    this list of conditions and the following disclaimer in the documentation
>    and/or other materials provided with the distribution.
>
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
> ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
> (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
> LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
> ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
> (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



BSD licenses are a family of permissive free software licenses, imposing minimal restrictions on the redistribution of software. This license allows unlimited redistribution for any purpose as long as its copyright notices and the license's disclaimers of warranty are maintained. BSD Licenses allow proprietary use and allow the software released under the license to be incorporated into proprietary products (i.e. works based on this material may be released under a proprietary license as closed source software). See [Wikipedia](https://en.wikipedia.org/wiki/BSD_licenses).


For instance, the [NOESIS framework for network data mining](https://bitbucket.org/fberzal/noesis) is an open-source project that relies on the **iKor library** of reusable components and this library is also used by other closed-source (or proprietary) software projects.